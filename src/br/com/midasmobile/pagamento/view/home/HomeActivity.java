package br.com.midasmobile.pagamento.view.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import br.com.midasmobile.pagamento.R;
import br.com.midasmobile.pagamento.view.ajuda.AjudaActivity;
import br.com.midasmobile.pagamento.view.historico.HistoricoActivity;
import br.com.midasmobile.pagamento.view.impressora.ImpressaoActivity;
import br.com.midasmobile.pagamento.view.leitor.LeitorActivity;
import br.com.midasmobile.pagamento.view.pagamento.PagamentoActivity;
import br.com.midasmobile.pagamento.view.sobre.SobreActivity;

public class HomeActivity extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		ImageView imgAjuda = (ImageView)findViewById(R.id.img_ajuda);
		imgAjuda.setOnClickListener(this);
		
		ImageView imgHistorico = (ImageView)findViewById(R.id.img_historico);
		imgHistorico.setOnClickListener(this);
		
		ImageView imgImprimir = (ImageView)findViewById(R.id.img_imprimir);
		imgImprimir.setOnClickListener(this);
		
		ImageView imgLeitor = (ImageView)findViewById(R.id.img_leitor);
		imgLeitor.setOnClickListener(this);
		
		ImageView imgPagamento = (ImageView)findViewById(R.id.img_pagamento);
		imgPagamento.setOnClickListener(this);
		
		ImageView imgSobre = (ImageView)findViewById(R.id.img_sobre);
		imgSobre.setOnClickListener(this);
		
	}
	
	
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.img_ajuda:
			Intent irParaAjuda = new Intent(this, AjudaActivity.class);
			startActivity(irParaAjuda);
			break;
		case R.id.img_historico:
			Intent irHistorico = new Intent(this, HistoricoActivity.class);
			startActivity(irHistorico);		
			break;
		case R.id.img_imprimir:
			Intent irParaImpressao = new Intent(this, ImpressaoActivity.class);
			startActivity(irParaImpressao);
			break;
		case R.id.img_leitor:
			Intent irParaLeitor = new Intent(this, LeitorActivity.class);
			startActivity(irParaLeitor);
			break;
		case R.id.img_pagamento:
			Intent irParaPagamento = new Intent(this, PagamentoActivity.class);
			startActivity(irParaPagamento);
			break;
		case R.id.img_sobre:
			Intent irParaSobre = new Intent(this, SobreActivity.class);
			startActivity(irParaSobre);
			break;

		default:
			break;
		}
	}

}
