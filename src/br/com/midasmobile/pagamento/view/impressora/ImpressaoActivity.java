package br.com.midasmobile.pagamento.view.impressora;

import java.util.Arrays;
import java.util.List;

import br.com.midasmobile.pagamento.R;
import br.com.midasmobile.pagamento.model.Impressora;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

public class ImpressaoActivity extends Activity{

	private ListView listaImpressoras;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.activity_impressora);
		
		this.listaImpressoras = (ListView)findViewById(R.id.lista_impressoras);
		carregarImpressoras();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		carregarImpressoras();
	}

	private void carregarImpressoras() {

		Impressora impressora01 = new Impressora(1L, "Google", "WebPrint");
		Impressora impressora02 = new Impressora(2L, "Lexmark", "XRT5");
		Impressora impressora03 = new Impressora(3L, "Digi Print", "K8000");
		List<Impressora> listaImpressoras = Arrays.asList(impressora01, impressora02, impressora03);
		
		ListaImpressorasAdapter listaImpressorasAdapter = new ListaImpressorasAdapter(listaImpressoras, this);
		
		this.listaImpressoras.setAdapter(listaImpressorasAdapter);
	}
}
