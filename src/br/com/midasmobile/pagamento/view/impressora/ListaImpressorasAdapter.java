package br.com.midasmobile.pagamento.view.impressora;

import java.util.List;

import br.com.midasmobile.pagamento.R;
import br.com.midasmobile.pagamento.model.Impressora;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListaImpressorasAdapter extends BaseAdapter{

	private List<Impressora> impressoras;
	private Activity activity;

	public ListaImpressorasAdapter(List<Impressora> impressoras, Activity activity){
		this.impressoras = impressoras;
		this.activity = activity;
	}
	
	@Override
	public int getCount() {
		return this.impressoras.size();
	}

	@Override
	public Object getItem(int posicao) {
		return this.impressoras.get(posicao);
	}

	@Override
	public long getItemId(int posicao) {
		return this.impressoras.get(posicao).getIdImpressora();
	}

	@Override
	public View getView(int posicao	, View convertView, ViewGroup parent){

		Impressora impressora = this.impressoras.get(posicao);
		
		
		LayoutInflater inflater = this.activity.getLayoutInflater();
		View linha = inflater.inflate(R.layout.linha_impressora, null);
		
		View fundo = linha.findViewById(R.id.fundo_linha_impressora);
//		if(posicao % 2 == 0){
//			fundo.setBackgroundColor( activity.getResources().getColor(R.color.linha_par) );
//		}else{
//			fundo.setBackgroundColor( activity.getResources().getColor(R.color.linha_impar) );
//		}
		
		ImageView imgImpressora = (ImageView)linha.findViewById(R.id.img_impressora);
		Drawable semFoto = activity.getResources().getDrawable(R.drawable.impressora);
		imgImpressora.setImageDrawable(semFoto);
		
		TextView fabricante = (TextView)linha.findViewById(R.id.label_fabricante_impressora);
		fabricante.setText("Fabricante: " + impressora.getFabricante());
		
		TextView modelo = (TextView)linha.findViewById(R.id.label_modelo_impressora);
		modelo.setText("Modelo: " + impressora.getModelo());
		
		return linha;
	}

}
