package br.com.midasmobile.pagamento.view.historico;

import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.midasmobile.pagamento.R;
import br.com.midasmobile.pagamento.model.Transacao;

public class ListaTransacoesAdapter extends BaseAdapter{

	private List<Transacao> transacoes;
	private Activity activity;

	public ListaTransacoesAdapter(List<Transacao> transacoes, Activity activity) {
		this.transacoes = transacoes;
		this.activity = activity;
	}

	@Override
	public int getCount() {
		return this.transacoes.size();
	}

	@Override
	public Object getItem(int posicao) {
		return this.transacoes.get(posicao);
	}

	@Override
	public long getItemId(int posicao) {
		return this.transacoes.get(posicao).getIdTransacao();
	}

	@Override
	public View getView(int posicao	, View convertView, ViewGroup parent) {
		
		Transacao transacao = this.transacoes.get(posicao);
		
		LayoutInflater inflater = this.activity.getLayoutInflater();
		View linha = inflater.inflate(R.layout.linha_transacao, null);
		
		
		View fundo = linha.findViewById(R.id.fundo_linha_transacao);
//		if(posicao % 2 == 0){
//			fundo.setBackgroundColor( activity.getResources().getColor(R.color.linha_par) );
//		}else{
//			fundo.setBackgroundColor( activity.getResources().getColor(R.color.linha_impar) );
//		}
		
		ImageView imgDiaCalendario = (ImageView)linha.findViewById(R.id.img_dia_calendario);
		Drawable semFoto = activity.getResources().getDrawable(R.drawable.calendario_v2);
		imgDiaCalendario.setImageDrawable(semFoto);
		
		TextView diaTransacao = (TextView)linha.findViewById(R.id.dia_transacao);
		diaTransacao.setText( getDia(transacao.getData()) );
		
		TextView mesTransacao = (TextView)linha.findViewById(R.id.mes_transacao);
		mesTransacao.setText( getMes(transacao.getData()) );
		
		TextView codigoTransacao = (TextView)linha.findViewById(R.id.codigo_transacao);
		codigoTransacao.setText("Transa��o: " + transacao.getIdTransacao().toString());
		
		TextView valorTransacao = (TextView)linha.findViewById(R.id.valor_transacao);
		valorTransacao.setText("R$" + transacao.getValor().toString());
		
		return linha;
	}

	private String getMes(Date data) {
		String mes = "n/d";
		switch (data.getMonth()) {
		case 0:
			mes = "Janeiro";
			break;
		case 1:
			mes = "Fevereiro";
			break;
		case 2:
			mes = "Mar�o";
			break;
		case 3:
			mes = "Abril";
			break;
		case 4:
			mes = "Maio";
			break;
		case 5:
			mes = "Junho";
			break;
		case 6:
			mes = "Julho";
			break;
		case 7:
			mes = "Agosto";
			break;
		case 8:
			mes = "Setembro";
			break;
		case 9:
			mes = "Outubro";
			break;
		case 10:
			mes = "Novembro";
			break;
		case 11:
			mes = "Dezembro";
			break;
		default:
			mes = "n/d";
			break;
		}
		
		return mes;
	}

	private String getDia(Date data) {
		java.util.Calendar cl = java.util.Calendar.getInstance();
		cl.setTime( data );
		int diaMes = cl.get(cl.DAY_OF_MONTH);
		return String.valueOf(diaMes);
	}
}
