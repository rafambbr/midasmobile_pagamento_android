package br.com.midasmobile.pagamento.view.historico;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;
import br.com.midasmobile.pagamento.R;
import br.com.midasmobile.pagamento.business.support.Extras;
import br.com.midasmobile.pagamento.model.Empresa;
import br.com.midasmobile.pagamento.model.Transacao;
import br.com.midasmobile.pagamento.view.impressora.ImpressaoActivity;

public class HistoricoActivity extends Activity implements OnItemLongClickListener{

	private ListView listaTransacoes;
	private Transacao transacaoSelecionada;

	private MenuItem menuItemImprimir;
	private MenuItem menuItemDetalhes;
	private MenuItem menuItemEstorno;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_historico);
		
		this.listaTransacoes = (ListView)findViewById(R.id.lista_trasacoes);
		this.listaTransacoes.setOnItemLongClickListener(this);
		super.registerForContextMenu(this.listaTransacoes);
		carregarTransacoes();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		carregarTransacoes();
	}

	private void carregarTransacoes() {
		
		Empresa barDoAlemao = new Empresa(1L, "Bar do Alem�o", "Av. logoAli, 128");
		Empresa zeDaPicanha = new Empresa(1L, "Z� da Picanha", "R. AliPerto, 375");
		
		Date dataHoje = new Date();
		
		Transacao transacao01 = new Transacao(1L, barDoAlemao, 200.30, dataHoje);
		Transacao transacao02 = new Transacao(2L, zeDaPicanha, 37.12, dataHoje);
		Transacao transacao03 = new Transacao(3L, barDoAlemao, 430.11, dataHoje);
		Transacao transacao04 = new Transacao(4L, zeDaPicanha, 12.55, dataHoje);
		List<Transacao> listaTransacoes = Arrays.asList(transacao01, transacao02, transacao03, transacao04);
		
		ListaTransacoesAdapter listaTransacoesAdapter = new ListaTransacoesAdapter(listaTransacoes, this);
		
		this.listaTransacoes.setAdapter(listaTransacoesAdapter);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> adapter, View view, int posicao, long id) {
		
		this.transacaoSelecionada = (Transacao)adapter.getItemAtPosition(posicao);
		
		return false; //true consome todo o evento n�o executando os demais listeners
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View view, ContextMenuInfo menuInfo) {
		
		super.onCreateContextMenu(menu, view, menuInfo);

		this.menuItemImprimir = menu.add("Imprimir");
		Intent imprimirTransacao = new Intent(this, ImpressaoActivity.class);
		imprimirTransacao.putExtra(Extras.TRANSACAO_SELECIONADA, this.transacaoSelecionada);
		menuItemImprimir.setIntent(imprimirTransacao);
		
		this.menuItemDetalhes = menu.add("Datalhes");
		
		this.menuItemEstorno = menu.add("Estorno");
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem menuItem) {
		
		super.onContextItemSelected(menuItem);
		
		int itemId = menuItem.getItemId();
		if(itemId  == this.menuItemImprimir.getItemId() ){
			imprimirTransacao(this.transacaoSelecionada);
		}else if( itemId == this.menuItemDetalhes.getItemId() ){
			exibirDetalheTransacao(this.transacaoSelecionada);
		}else if( itemId == this.menuItemEstorno.getItemId() ){
			estornoDaTransacao(this.transacaoSelecionada);
		}

		return false;
	}
	
	private void imprimirTransacao(Transacao transacaoSelecionada) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "Imprimir Transa��o Selecionada", Toast.LENGTH_SHORT).show();
	}

	private void exibirDetalheTransacao(Transacao transacaoSelecionada) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "Exibir Detalhe Transa��o Selecionada", Toast.LENGTH_SHORT).show();
	}
	
	private void estornoDaTransacao(Transacao transacaoSelecionada) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "Estorno da Transa��o Selecionada", Toast.LENGTH_SHORT).show();
	}
}
