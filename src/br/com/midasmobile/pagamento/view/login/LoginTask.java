package br.com.midasmobile.pagamento.view.login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

public class LoginTask extends AsyncTask<Integer, Double, String> {

	private Activity context;
	private ProgressDialog progressDialog;

	public LoginTask(Activity context){
		this.context = context;
	}
	
	/**
	 * Todo c�digo rodado nesse m�todo � executado na Thread principal da UI
	 * � executado antes do m�todo "doInBackground()" ter sido executado
	 */
	@Override
	protected void onPreExecute() {
		progressDialog = ProgressDialog.show(context, "Aguarde...", "Enviando dados para web...", true, true);
	}
	
	/**
	 * AsyncTask executa tarefas pesadas fora da Thread principal da UI
	 */
	@Override
	protected String doInBackground(Integer... params) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Todo c�digo rodado nesse m�todo � executado na Thread principal da UI
	 * � executado ap�s o m�todo "doInBackground()" ter sido executado
	 */
	@Override
	protected void onPostExecute(String result) {
	
		progressDialog.dismiss(); //Libera o "ProgressDialog" ap�s a execu��o terminar
		Toast.makeText(context, "JSON: " + result, Toast.LENGTH_LONG).show();
	}


}
