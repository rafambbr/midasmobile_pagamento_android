package br.com.midasmobile.pagamento.view.login;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import br.com.midasmobile.pagamento.R;

public class LoginHelper {

	private LoginActivity loginActivity;
	
	private TextView labelEmail;
	private EditText inputEmail;
	private TextView labelSenha;
	private EditText inputSenha;
	private Button btnLogin;
	private Button btnAjuda;

	public LoginHelper(LoginActivity loginActivity){
		this.loginActivity = loginActivity;
		
		this.labelEmail = (TextView)loginActivity.findViewById(R.id.label_email);
		this.inputEmail = (EditText)loginActivity.findViewById(R.id.input_email);
		this.labelSenha = (TextView)loginActivity.findViewById(R.id.label_senha);
		this.inputSenha = (EditText)loginActivity.findViewById(R.id.input_senha);
		this.btnLogin = (Button)loginActivity.findViewById(R.id.btn_login);
		this.btnAjuda = (Button)loginActivity.findViewById(R.id.btn_ajuda);
	}

	public LoginActivity getLoginActivity() {
		return loginActivity;
	}

	public TextView getLabelEmail() {
		return labelEmail;
	}

	public EditText getInputEmail() {
		return inputEmail;
	}

	public TextView getLabelSenha() {
		return labelSenha;
	}

	public EditText getInputSenha() {
		return inputSenha;
	}

	public Button getBtnLogin() {
		return btnLogin;
	}

	public Button getBtnAjuda() {
		return btnAjuda;
	}
	
	
	
}
