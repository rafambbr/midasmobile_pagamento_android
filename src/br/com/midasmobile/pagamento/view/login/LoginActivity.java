package br.com.midasmobile.pagamento.view.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import br.com.midasmobile.pagamento.R;
import br.com.midasmobile.pagamento.view.home.HomeActivity;

public class LoginActivity extends Activity implements OnClickListener{

	private LoginHelper loginHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		this.loginHelper = new LoginHelper(this);
		this.loginHelper.getBtnLogin().setOnClickListener(this);
		this.loginHelper.getBtnAjuda().setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btn_login:
			
			Intent irParaHome = new Intent(this, HomeActivity.class);
			startActivity(irParaHome);
			
			break;
		case R.id.btn_ajuda:
			
			break;

		default:
			break;
		}
		
	}


}
