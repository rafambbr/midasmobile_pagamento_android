package br.com.midasmobile.pagamento.model;

import java.io.Serializable;

public class Usuario implements Serializable {

	private static final long serialVersionUID = -8021138004525774028L;
	
	private String email;
	private String senha;
	
	public Usuario(){
		//No-op
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
}
