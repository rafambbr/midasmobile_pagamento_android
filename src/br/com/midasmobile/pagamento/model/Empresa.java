package br.com.midasmobile.pagamento.model;

import java.io.Serializable;

public class Empresa implements Serializable{

	private static final long serialVersionUID = 6961722197945569643L;
	
	private Long idEmpresa;
	private String nome;
	private String endereco;
	
	public Empresa(){
		//No-op
	}

	public Empresa(Long idEmpresa, String nome, String endereco) {
		this.idEmpresa = idEmpresa;
		this.nome = nome;
		this.endereco = endereco;
	}



	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
}
