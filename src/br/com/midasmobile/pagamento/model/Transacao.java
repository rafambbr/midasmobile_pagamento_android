package br.com.midasmobile.pagamento.model;

import java.io.Serializable;
import java.util.Date;

public class Transacao implements Serializable {

	private static final long serialVersionUID = 4407967830588033718L;
	
	private Long idTransacao;
	private Empresa empresa;
	private Double valor;
	private Date data;
	
	public Transacao(){
		//No-op
	}

	public Transacao(Long idTransacao, Empresa empresa, Double valor, Date data) {
		this.idTransacao = idTransacao;
		this.empresa = empresa;
		this.valor = valor;
		this.data = data;
	}

	public Long getIdTransacao() {
		return idTransacao;
	}

	public void setIdTransacao(Long idTransacao) {
		this.idTransacao = idTransacao;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	
}
