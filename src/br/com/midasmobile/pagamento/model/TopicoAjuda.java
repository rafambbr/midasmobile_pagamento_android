package br.com.midasmobile.pagamento.model;

import java.io.Serializable;

public class TopicoAjuda implements Serializable{

	private static final long serialVersionUID = -8529392114043280422L;

	private Long idTopicoAjuda;
	private String titulo;
	private String descricao;
	
	public TopicoAjuda(){
		//No-op
	}
}
