package br.com.midasmobile.pagamento.model;

import java.io.Serializable;

public class Impressora implements Serializable{

	private static final long serialVersionUID = 7921959225356027986L;
	
	private Long idImpressora;
	private String fabricante;
	private String modelo;
	
	public Impressora(){
		//No-op
	}

	public Impressora(Long idImpressora, String fabricante, String modelo) {
		super();
		this.idImpressora = idImpressora;
		this.fabricante = fabricante;
		this.modelo = modelo;
	}

	public Long getIdImpressora() {
		return idImpressora;
	}

	public void setIdImpressora(Long idImpressora) {
		this.idImpressora = idImpressora;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

}
